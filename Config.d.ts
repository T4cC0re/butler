interface Config {
    token: string; // Discord bot token
    guild: string; // Doscird guild/server ID
    dev_mode ?: boolean; // Only run dev commands (and NOTHING else)
    prometheus?: { // Configure prometheus pushgateway for metrics if desired.
        job_name: string;
        gateway: string;
        username?: string; // If provided basic auth will be used
        password ?: string;
        expose?: boolean;
    };
    twitch: { // Twitch follow integration
        follow_stream: string; // channel name to follow
        clientId: string;
        clientSecret: string;
        voiceChannel: string;
        announce?: string; // Channel to post stream announcements in. If not provided global announcements channel is used
    };
    channels: string[]; // Channels to add to the 'channel' type for commands
    meme_channels: string[]; // Channels to add to the 'meme' type for commands
    announcements: string; // Channel to use as 'announcements' type for commands
    dev_command_roles: string[]; // Roles to use to grant access to dev commands
    auto_react_ids: { [key: string]: string } // Key value map of UIDs/RIDs to Emotes (UTF-8 emoji, or server emotes with `!` prefix. e.g. `!myemote`)
}
