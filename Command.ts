import {Message} from "discord.js";
import {MessageHandler} from "./MessageHandler";

export type ButlerChannelType = 'dm' | 'meme' | 'announcements' | 'rules' | 'channel' | 'not_on_list';

export interface Command {
    name: string
    description?: string
    showInList?: boolean
    devOnly?: boolean
    adminOnly?: boolean
    allowedSources?: ButlerChannelType[]
    match?: RegExp
    exactMatch?: string
    alwaysRunIfPermsAllow?: boolean
    continueAfterExec?: boolean
    exec: (handler: MessageHandler, message: Message) => Promise<any>
}
