import {Client, Guild} from 'discord.js';
import {MessageHandler} from './MessageHandler.js';
import {readFile} from 'fs/promises';
import YAML from 'yaml';
import {commands} from './commands.js';
import {TwitchFollower} from "./TwitchFollower.js";
import promClient from 'prom-client';
import express from "express";

const {collectDefaultMetrics, Gauge, Pushgateway, register} = promClient;

const alive = new Gauge<string>({
    name: "butler_alive",
    help: "Timestamp of last checkin"
});
alive.reset();
register.registerMetric(alive);
collectDefaultMetrics();

export class Butler {
    private readonly client: Client;
    private handler: MessageHandler;
    private guild: Guild;
    private readonly token: string;
    private twitch: TwitchFollower;
    private gateway: promClient.Pushgateway;

    constructor(private config: Config) {
        const server = express();
        if (!this.config.token) {
            console.error("No token!");
            throw "no token";
        }
        if (this.config.prometheus?.gateway) {
            if (this.config.dev_mode) {
                console.log(`[DEV MODE] skipping prometheus setup`);
            } else {
                let auth: any;
                if (this.config.prometheus.username) {
                    auth = `${this.config.prometheus.username}:${this.config.prometheus.password}`
                }
                this.gateway = new Pushgateway(this.config.prometheus.gateway, {auth: auth}, null);
            }
        }
        if (this.config.prometheus?.expose) {
            server.get('/metrics', async (req, res) => {
                try {
                    res.set('Content-Type', register.contentType);
                    res.end(await register.metrics());
                } catch (ex) {
                    res.status(500).end(ex);
                }
            });
        }

        this.token = this.config.token;

        this.client = new Client();

        this.client.on('ready', () => {
            console.log(`Logged in as ${this.client.user.tag}!`);
            this.guild = this.client.guilds.resolve(this.config.guild)

            console.log(`Guild is ${this.guild.name}!`);

            this.twitch = new TwitchFollower(this.client, this.config);
            this.handler = new MessageHandler(this.guild, this.twitch, this.config, commands);

            this.client.on('message', this.handler.handleMessage);
            this.client.on('messageUpdate', async (old, newM) => {
                return this.handler.handleMessage(await (newM.fetch()));
            });
            this.client.on('emojiCreate', this.handler.refresh);
            this.client.on('emojiDelete', this.handler.refresh);
            this.client.on('emojiUpdate', this.handler.refresh);
            this.client.on('guildMemberUpdate', this.handler.refresh);

            // No arrow function because of scoping inside the gauge
            this.client.setInterval(
                function () {
                    alive.setToCurrentTime();
                },
                1_000
            );

            if (this.gateway) {
                this.client.setInterval(
                    async () => {
                        const gateway = this.gateway;
                        const name = this.config.prometheus?.job_name

                        function scopingShield() {
                            gateway.push({jobName: name}, function (error, httpResponse, body) {
                                if (error) {
                                    console.error(error);
                                }
                            });
                        }

                        scopingShield();
                    },
                    10_000
                );
            } else {
                console.log('prometheus not configured. Not pushing metrics');
            }
        });

        this.client.login(this.token);
        const run = async() => {
            const port = 3000;
            console.log(
                `Server listening to ${port}, metrics exposed on /metrics endpoint`,
            );
            server.listen(port);
        };
        run();
    }
}

const config: Config = YAML.parse(await readFile('./config.yml', 'utf8'));

new Butler(config);
