import {Client, MessageEmbed, VoiceChannel} from "discord.js";
import {ApiClient, HelixUser, Stream} from 'twitch';
import {ClientCredentialsAuthProvider} from 'twitch-auth';
import promClient from 'prom-client';
import {getStream} from 'twitch-m3u8';

const {Gauge, register} = promClient;

const online = new Gauge<string>({
    name: "butler_twitch_online",
    help: "Amount of messages sent on the server",
    labelNames: ['twitch_channel'],
});

online.reset();
register.registerMetric(online);

export class TwitchFollower {
    private twitch: ApiClient;
    private isOnline: boolean;
    private streamTitle: string;
    private helixUser: HelixUser;
    private initialized: boolean;
    private callback: TwitchCallback;

    constructor(private client: Client, private config: Config) {
        const authProvider = new ClientCredentialsAuthProvider(this.config.twitch.clientId, this.config.twitch.clientSecret);
        this.twitch = new ApiClient({authProvider});
        this.check();
        this.client.setInterval(
            this.check,
            15_000
        );
    }

    public check = async () => {
        let changed = false;
        if (!this.initialized) {
            this.helixUser = await this.twitch.helix.users.getUserByName(this.config.twitch.follow_stream)
        }
        try {
            const stream = await this.twitch.kraken.streams.getStreamByChannel(this.helixUser.id);
            if (stream && stream.type === 'live') {
                online.labels(this.helixUser.name).set(1);
                changed = !(this.isOnline && this.streamTitle === stream.channel.status);

                if (changed) {
                    await this.setOnline(stream);
                }
            } else {
                online.labels(this.helixUser.name).set(0);
                changed = !(!this.isOnline && this.initialized);

                if (changed) {
                    await this.setOffline();
                }
            }
        } catch (e) {
            console.error(e);
        }

        if (changed && this.isOnline && this.callback) {
            this.playStream();
            if (this.config.dev_mode) {
                console.log(`[DEV MODE] skipping online notification`);
            } else {
                return this.callback(await this.renderEmbed());
            }
        }
    }

    public setOnline = async (stream: Stream) => {
        console.log(`Twitch stream online! ${stream.channel.status} @ ${stream.channel.displayName}`);
        this.isOnline = true;
        this.streamTitle = stream.channel.status;
        this.initialized = true;
        return this.client.user.setPresence(
            {
                activity: {
                    name: stream.channel.status,
                    type: 'STREAMING',
                    url: stream.channel.url,
                }
            }
        );
    }

    public setOffline = async () => {
        if (!this.isOnline && this.initialized) {
            return
        }
        console.log(`Twitch stream offline! ${this.config.twitch.follow_stream}`);
        this.isOnline = false;
        this.streamTitle = `${this.helixUser.displayName} ist offline`;
        this.initialized = true;
        return this.client.user.setPresence(
            {
                activity: {
                    name: 'Apfelspüli',
                    type: 'PLAYING',
                }
            }
        );
    }

    public renderEmbed = async (broadcaster ?: string): Promise<MessageEmbed> => {
        if (!this.initialized) {
            await this.check();
        }

        if (broadcaster) {
            broadcaster = broadcaster.trim();
        }

        let helixUser: HelixUser;
        let isOnline: boolean;
        let streamTitle: string;
        if (broadcaster) {
            helixUser = await this.twitch.helix.users.getUserByName(broadcaster);
            const stream = await this.twitch.kraken.streams.getStreamByChannel(helixUser.id);
            if (stream && stream.type === 'live') {
                isOnline = true;
                streamTitle = stream.channel.status;
            } else {
                isOnline = false;
                streamTitle = `${this.helixUser.displayName} ist offline`;
            }
        } else {
            helixUser = this.helixUser;
            isOnline = this.isOnline;
            streamTitle = this.streamTitle;
        }

        const message = new MessageEmbed();
        message.setTitle(streamTitle);
        message.setColor(isOnline ? 'GREEN' : 'RED');
        message.setDescription(helixUser.description);
        message.setAuthor(helixUser.displayName, helixUser.profilePictureUrl, `https://www.twitch.tv/${helixUser.name}`);
        message.setThumbnail((await helixUser.getStream())?.thumbnailUrl || helixUser.offlinePlaceholderUrl || helixUser.profilePictureUrl);
        message.setURL(isOnline ? `https://www.twitch.tv/${helixUser.name}` : null);
        message.addField("Views", `${helixUser.views}`);
        message.setFooter(`${helixUser.displayName} on Twitch`);
        return message;
    };

    public playStream = async (broadcaster?: string): Promise<any> => {
        if (!this.config.twitch.voiceChannel) {
            console.log("no voice Channel")
            return;
        }
        const streams = await getStream(broadcaster || this.helixUser.name);
        console.log(streams);

        let streamUrl: string;
        for (let stream of streams) {
            if (stream.quality == "audio_only") {
                streamUrl = stream.url;
                break;
            }

            streamUrl = stream.url;
            // Continue if not audio only stream
        }

        if (!streamUrl || !streams.length) {
            console.log("could not find stream URL");
            return;
        }

        console.log('url:', streamUrl);

        const vc: VoiceChannel = await this.client.channels.fetch(this.config.twitch.voiceChannel) as any;
        const conn = await vc.join();
        const dispatcher = conn.play(streamUrl);
        dispatcher.pause();
        console.log('pause');
        dispatcher.resume();
        console.log('resume');

        dispatcher.on('debug', (a) => {
            console.log(a);
        });
        dispatcher.on('error', (e) => {
            console.log(e);
            dispatcher.destroy();
            vc.leave();
        });
        dispatcher.on('finish', () => {
            console.log('Finished playing!');
            dispatcher.destroy(); // end the stream
            vc.leave();
        });
    }

    public registerCallback = async (callback: TwitchCallback) => {
        this.callback = callback;
    };
}

export type TwitchCallback = (embed: MessageEmbed) => Promise<any>;
