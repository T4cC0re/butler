import {
    Client,
    Emoji,
    EmojiIdentifierResolvable,
    Guild,
    Message,
    MessageEmbed,
    ReactionEmoji,
    Snowflake,
    TextChannel
} from "discord.js";
import {PermissionCheck} from "./PermissionCheck.js";
import {ButlerChannelType, Command} from "./Command.js";
import {TwitchFollower} from "./TwitchFollower.js";
import promClient from 'prom-client';

const {Counter, Gauge, register} = promClient;

const messages = new Counter<string>({
    name: "butler_server_messages",
    help: "Amount of messages sent on the server",
    labelNames: ['userID', 'channel'],
});
const commands = new Counter<string>({
    name: "butler_server_commands",
    help: "Amount of commands executed (excludes always ran)",
    labelNames: ['userID', 'channel', 'command'],
});
const users = new Gauge<string>({
    name: "butler_user_name",
    help: "Name of user",
    labelNames: ['userID', 'name'],
});
const channels = new Gauge<string>({
    name: "butler_channel_name",
    help: "Name of channel",
    labelNames: ['userID', 'name'],
});

messages.reset();
commands.reset();
users.reset();
channels.reset();
register.registerMetric(messages);
register.registerMetric(commands);
register.registerMetric(users);
register.registerMetric(channels);

export class MessageHandler {
    private emojiCache: Map<string, ReactionEmoji> = new Map<string, ReactionEmoji>();
    public permissions: PermissionCheck;
    private client: Client;
    private commands: Map<ButlerChannelType, Command[]> = new Map<ButlerChannelType, Command[]>();

    constructor(public guild: Guild, public twitch: TwitchFollower, public config: Config, public all_commands: Command[]) {
        this.client = this.guild.client;
        this.permissions = new PermissionCheck(this.guild, this.config);
        for (let command of all_commands) {
            if (!command.allowedSources) {
                command.allowedSources = ['dm', 'meme', 'announcements', 'rules', 'channel', 'not_on_list'];
            }
            console.log(`adding ${command.name} to ${command.allowedSources.join(', ')}`);

            for (let source of command.allowedSources) {
                let cmds: Command[] = this.commands.get(source) || []
                cmds.push(command);
                this.commands.set(source, cmds);
            }
        }
        this.client.setInterval(this.refresh, 300_000);
        this.twitch.registerCallback(this.postTwitchStatus)
        console.log('Ready to rumble');
    }

    private postTwitchStatus = async (embed: MessageEmbed): Promise<any> => {
        const target = this.config.twitch.announce || this.config.announcements;
        const channel: TextChannel = await this.client.channels.fetch(target) as TextChannel;
        return channel.send(embed);
    }

    public refresh = async () => {
        this.emojiCache.clear();
        await this.permissions.refresh();
    }

    public lookupEmoji = async (code: string): Promise<ReactionEmoji | null> => {
        if (!this.emojiCache.has(code.toLowerCase())) {
            this.client.emojis.valueOf().map((emoji) => {
                this.emojiCache.set(emoji.name.toLowerCase(), emoji as Emoji as ReactionEmoji);
            });
        }

        return this.emojiCache.get(code.toLowerCase());
    }

    private channelType = (message: Message): ButlerChannelType => {
        if (message.channel.type == 'dm') {
            return 'dm';
        }

        if (this.config.meme_channels.includes(message.channel.id)) {
            return 'meme';
        }

        if (this.config.announcements === message.channel.id) {
            return 'announcements';
        }

        if (this.guild.rulesChannelID === message.channel.id) {
            return 'rules';
        }

        if (this.config.channels.includes(message.channel.id)) {
            return 'channel';
        }

        return 'not_on_list';
    }

    public handleMessage = async (message: Message) => {
        if (message.author.bot || message.author.system) {
            return
        }
        messages.inc({userID: message.author.id, channel: message.channel.id})
        users.labels(message.author.id, message.author.tag).set(1);

        try {
            if (message.channel.type == 'dm') {
                channels.labels(message.channel.id, 'DM').set(1);
            } else {
                channels.labels(message.channel.id, (message.channel as TextChannel).name).set(1)
            }
        } catch (e) {
            console.log(`could not update channel metric for ${message.channel}`, e);
        }

        try {
            const source = this.channelType(message)
            // console.log(`message ${message.id} source: ${source}`);
            for (let command of this.commands.get(source)) {
                // console.log(`checking command ${command.name}`);

                if (command.alwaysRunIfPermsAllow || command.exactMatch === message.content || (command.match ? message.content.match(command.match) : false)) {
                    // console.log(`command ${command.name} matches message ${message.id}`);
                } else {
                    // console.log(`command ${command.name} did not match message ${message.id}`);
                    continue;
                }

                // This gates away dev instances from any interaction
                if (this.config.dev_mode && !command.devOnly) {
                    console.log(`[DEV MODE] skipping non-dev command ${command.name} for ${message.id}`);
                    continue;
                }

                if (command.devOnly) {
                    // console.log(`command ${command.name} requires a dev_command_role`);
                    if (!await this.permissions.hasDevCommandsAccess(message.author.id)) {
                        console.error(`message author ${message.author.id} does not have a dev_command_role`);
                        await message.react("❌")
                        continue;
                    }
                }

                if (command.adminOnly) {
                    // console.log(`command ${command.name} requires admin priviliges`);
                    if (!await this.permissions.isAdmin(message.author.id)) {
                        console.error(`message author ${message.author.id} does not have admin priviliges`);
                        await message.react("❌")
                        continue;
                    }
                }

                try {
                    await command.exec(this, message);
                    if (!command.alwaysRunIfPermsAllow) {
                        console.log(`message ${message.id} handled via ${command.name}`);
                        commands.inc({userID: message.author.id, channel: message.channel.id, command: command.name});
                    }
                } catch (e) {
                    console.error(`message ${message.id} errored via ${command.name}`, e);
                }

                if (!command.continueAfterExec) {
                    return;
                }
            }
        } catch (e) {
            if (e) {
                console.error(e);
                message.react("🔥")
            }
        }
    }

    public getEmojiForID = async (id: Snowflake): Promise<EmojiIdentifierResolvable> => {
        let emoji = null;

        if (!this.config.auto_react_ids[id]) {
            return;
        }

        try {
            if (this.config.auto_react_ids[id].match('!')) {
                // console.log(`looking up emote for ${id}. ${this.config.auto_react_ids[id]}`);
                emoji = await this.lookupEmoji(this.config.auto_react_ids[id].substring(1));
            } else {
                emoji = this.config.auto_react_ids[id];
            }
        } catch (_) {
        }
        console.log(`emote for ${id}: ${emoji}`);

        return emoji;
    };
}
