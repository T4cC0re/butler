import {Guild, Role, Snowflake} from "discord.js";

export class PermissionCheck {
    /**
     * Map of <role ID, user ID>
     * @private
     */
    private roleCache: Map<string, string[]> = new Map<string, string[]>()

    constructor(private guild: Guild, private config: Config) {
    }

    public refresh = async () => {
        this.roleCache.clear()
        for (let role of this.config.dev_command_roles) {
            await this.addToCache(role)
        }
    }

    private addToCache = async (role: Snowflake) => {
        let gRole: Role
        try {
            gRole = await this.guild.roles.fetch(role)
        } catch (_) {
            return;
        }

        let memberIds: string[] = [];
        gRole.members.each(member => {
            memberIds.push(member.id)
        });
        this.roleCache.set(gRole.id, memberIds)
    }

    public isAdmin = async (memberId: Snowflake): Promise<boolean> => {
        return (await this.guild.member(memberId).fetch(true)).hasPermission('ADMINISTRATOR')
    }

    public hasDevCommandsAccess = async (memberId: Snowflake): Promise<boolean> => {
        return this.memberHasRole(memberId, ...this.config.dev_command_roles);
    }

    public memberHasRole = async (memberId: Snowflake, ...roles: Snowflake[]): Promise<boolean> => {
        for (let role of roles) {
            console.log(role)
            if (!this.roleCache.has(role)) {
                await this.addToCache(role)
            }

            if (this.roleCache.get(role).includes(memberId)) {
                return true;
            }
        }

        return false;
    }
}