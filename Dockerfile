FROM node:14-alpine
WORKDIR /app
ENTRYPOINT node butler.js
RUN apk --no-cache add ffmpeg
ARG VERSION=source
COPY . /app
RUN apk add --no-cache --virtual .gyp python3 ffmpeg make g++ \
    && npm install --also=dev . \
    && apk del .gyp
RUN npx tsc
RUN find . -name '*.ts' -delete
