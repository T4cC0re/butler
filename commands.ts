import {Command} from "./Command";
import {MessageEmbed} from "discord.js";
import promClient from "prom-client";
import ytdl from "discordjs-ytdl-advanced";

const {Counter, register} = promClient;

const leibe = new Counter<string>({
    name: "butler_leibe",
    help: "Amount of leibe corrections distributed",
    labelNames: ['userID'],
});
leibe.reset();
register.registerMetric(leibe);
const aehre = new Counter<string>({
    name: "butler_aehre",
    help: "Amount of aehre corrections distributed",
    labelNames: ['userID'],
});
aehre.reset();
register.registerMetric(aehre);

export const commands: Command[] = [
    {
        name: `dummy`,
        description: `copy paste`,
        showInList: false,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm', 'announcements', 'rules'],
        continueAfterExec: false,
        match: null,
        exactMatch: '!dummy',
        devOnly: false,
        adminOnly: false,
        alwaysRunIfPermsAllow: false,
        exec: async (handler, message) => {
        },
    },
    {
        name: `env`,
        description: `Environment information`,
        showInList: false,
        allowedSources: ['dm'],
        exactMatch: '!env',
        adminOnly: true,
        exec: async (handler, message) => {
            const reply = new MessageEmbed();

            for (let key in process.env) {
                reply.addField(key, process.env[key] || '<empty>')
            }

            return message.reply(reply);
        },
    },
    {
        name: `admin`,
        description: `admin test`,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm', 'announcements', 'rules'],
        continueAfterExec: false,
        match: null,
        exactMatch: '!admin',
        adminOnly: true,
        alwaysRunIfPermsAllow: false,
        exec: async (handler, message) => {
            try {
                const dm = await message.author.createDM();
                await dm.send(`This is a personal reply to ${message.url}`);
                await message.react("✔")
            } catch (e) {
                console.error(e);
                await message.react("🔥")
            }
        },
    },
    {
        name: `dev`,
        description: `dev test`,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm', 'announcements', 'rules'],
        continueAfterExec: false,
        match: null,
        exactMatch: '!dev',
        devOnly: true,
        alwaysRunIfPermsAllow: false,
        exec: async (handler, message) => {
        },
    },
    {
        name: `play_twitch [twitch name]`,
        description: `Spiele einen Twitch stream im Musik Voice channel. Wenn keiner gegeben ist, \`MoreCoreDE\``,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme'],
        match: /^!play_twitch ?(.*)/,
        devOnly: false,
        exec: async (handler, message) => {
            const broadcaster = message.content.replace('!play_twitch', '').trim();
            try {
                handler.twitch.playStream(broadcaster);
                return message.channel.send(
                    `${message.author}:`,
                    await handler.twitch.renderEmbed(broadcaster)
                );
            } catch (e) {
                return message.reply('Oops. Das ist ein fehler passiert :grimacing:')
            }
        },
    },
    {
        name: `play [link]`,
        description: `Spielt einen Link sofort ab. (Keine Warteschlange)`,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme'],
        match: /^!play ?(.*)/,
        devOnly: false,
        exec: async (handler, message) => {
            const link = message.content.replace('!play', '').trim();

            if (message.member.voice.channel) {
                const connection = await message.member.voice.channel.join();
                const SONG = await ytdl(link)
                const song = SONG.play(connection)
                message.reply(`Spiele ${link}...`);
            } else {
                message.reply('You need to join a voice channel first!');
            }
        },
    },
    {
        name: `twitch [twitch name]`,
        description: `Zeigt an ob gerade etwas gestreamed wird. Wenn keiner gegeben ist, \`MoreCoreDE\``,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm'],
        match: /^!twitch ?(.*)/,
        devOnly: false,
        exec: async (handler, message) => {
            const broadcaster = message.content.replace('!twitch', '').trim();
            return message.channel.send(
                `${message.author}:`,
                await handler.twitch.renderEmbed(broadcaster)
            );
        },
    },
    {
        name: `commands`,
        description: `Zeigt alle commands an`,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm'],
        exactMatch: '!commands',
        exec: async (handler, message) => {
            const reply = new MessageEmbed();
            reply.setTitle('Command liste');
            reply.setDescription(``);
            reply.setColor('BLUE');
            reply.setFooter(``)
            for (let cmd of handler.all_commands) {
                if (!cmd.showInList) {
                    continue;
                }

                let description = cmd.description || "";

                if (cmd.devOnly) {
                    if (await handler.permissions.hasDevCommandsAccess(message.author.id) && message.channel.type === 'dm') {
                        description += `\n(Bot Tester only)`
                    } else {
                        continue
                    }
                }

                if (cmd.adminOnly) {
                    if (await handler.permissions.isAdmin(message.author.id) && message.channel.type === 'dm') {
                        description += `\n(admin only)`
                    } else {
                        continue
                    }
                }

                reply.addField(cmd.exactMatch || cmd.name, description);
            }

            return Promise.all(
                [
                    message.react("🤖"),
                    message.channel.send(`Hey ${message.author}!\nDiese commands kannst du per chat ausführen.\nEinige commands funktionieren nicht überall 🙃.\nSchreibe \`!commands\` in einer DM an ${message.client.user} für alle commands die deinem user zur Verfügung stehen.`, reply)
                ]
            );
        },
    },
    {
        name: `help`,
        description: `Verschafft einen kleinen Überblick was du hier tun kannst`,
        showInList: true,
        allowedSources: ['not_on_list', 'channel', 'meme', 'dm'],
        continueAfterExec: false,
        match: null,
        exactMatch: '!help',
        devOnly: false,
        adminOnly: false,
        alwaysRunIfPermsAllow: false,
        exec: async (handler, message) => {
            const reply = new MessageEmbed();
            reply.setTitle("Hilfe? Stets zu diensten!");
            reply.setFooter(`${handler.guild.name} steht in keinem offiziellen Zusammenhang mit MoreCore.`);
            reply.setColor('GREEN');
            reply.addField(`Was'n das hier?`, 'Ein hilfsbereiter 🤖 Roboter 🙃');
            reply.addField('Regeln?', `<#${handler.guild.rulesChannelID}>`);
            reply.addField('Ankündigungen?', `<#${handler.config.announcements}>`);
            reply.addField(`Was kann der bot?`, `Schreibe \`!commands\` in den chat und finde es heraus 😎`)
            return Promise.all(
                [
                    message.react("🥺"),
                    message.channel.send(`Hey ${message.author}! Hier ein kleiner Überblick was du hier tun kannst`, reply)
                ]
            );
        },
    },
    {
        name: `auto reactions`,
        description: `Reagiert auf verschiedene Dinge wie von Geisterhand`,
        continueAfterExec: true,
        alwaysRunIfPermsAllow: true,
        exec: async (handler, message) => {
            let reactions: Promise<any>[] = [];

            if (message.content.match(/b(a|ä)m/gmi)) {
                const hotbutton = await handler.lookupEmoji('morecoreHotbutton');
                await message.react(hotbutton);
            }

            if (message.content.match(/apfelsp(ue|ü)li/gmi)) {
                const apfelspueli = await handler.lookupEmoji('apfelspueli');
                reactions.push(message.react(apfelspueli));
            }

            if (message.content.match(/morecore/gmi)) {
                const hotbutton = await handler.lookupEmoji('morecoreHotbutton');
                reactions.push(message.react(hotbutton));
            }

            if (message.content.match(/(miau|meow)/gmi)) {
                reactions.push(message.react("😺"));
            }

            if (message.content.match(/(lost|morris)/gmi)) {
                const morris = await handler.lookupEmoji('morris');
                reactions.push(message.react(morris));
            }

            if (message.content.match(/(tacc?o)/gmi)) {
                reactions.push(message.react("🌮"));
            }

            if (message.content.match(/(rah?men)/gmi)) {
                reactions.push(message.react("🍜"));
            }

            if (message.content.match(/\bl(ie|ei)be/gmi)) {
                reactions.push(message.react("❤"));
            }

            if (message.content.match(/loop( +loop|core)/gmi)) {
                const loopcore = await handler.lookupEmoji('loopcore');
                reactions.push(message.react(loopcore));
            }

            if (message.content.match(/stitch/gmi)) {
                const stitch = await handler.lookupEmoji('stitch');
                reactions.push(message.react(stitch));
            }

            if (message.mentions?.members) {
                for (let member of message.mentions.members.values()) {
                    const emoji = await handler.getEmojiForID(member.id);
                    if (emoji) {
                        reactions.push(message.react(emoji));
                    }
                }
            }

            if (message.mentions?.roles) {
                for (let role of message.mentions.roles.values()) {
                    const emoji = await handler.getEmojiForID(role.id);
                    if (emoji) {
                        reactions.push(message.react(emoji));
                    }
                }
            }

            return Promise.all(reactions)
        },
    },
    //TODO: NR4
    //TODO: Zahn
    //TODO: So Leute
    //TODO: Hardfloor
    //TODO: lost
    {
        name: `loop loop`,
        description: `loop loop`,
        match: /\b(wie|was)\smacht\sder\szug\b/gmi,
        continueAfterExec: true,
        exec: async (handler, message) => {
            const msg = await message.channel.send("LOOP LOOP 🚂");
            const loopcore = await handler.lookupEmoji('loopcore');
            return Promise.all([message.react(loopcore), msg.react(loopcore)]);
        },
    },
    {
        name: `emo`,
        description: `emo`,
        match: /\bemo/gmi,
        continueAfterExec: true,
        exec: async (handler, message) => {
            const emo = await handler.lookupEmoji('emo');
            return message.react(emo);
        },
    },
    {
        name: `lost`,
        description: `lost`,
        match: /\blost\b/gmi,
        continueAfterExec: true,
        exec: async (handler, message) => {
            const counter = aehre.labels(message.author.id)
            const regex = /\blost\b/gmi;
            const matches = message.content.matchAll(regex);

            let corrections: string[] = [];
            for (let match of matches) {
                if (!match) {

                }
                corrections.push(`shööl`);
            }

            let reply: string;
            if (corrections.length == 1) {
                reply = `*${corrections[0]}`
            } else {
                reply = `\n*${corrections.join(`\n*`)}`
            }

            counter.inc(corrections.length);

            return message.reply(reply);
        },
    },
    {
        name: `Ähre`,
        description: `Denn \`Ähre\` ist einfach wichtig`,
        match: /\behre/gmi,
        continueAfterExec: true,
        exec: async (handler, message) => {
            const counter = aehre.labels(message.author.id)
            const regex = /\b(e)hre(:?[^\s]*)\b/gmi;

            const matches = message.content.matchAll(regex)

            let corrections: string[] = []
            for (let match of matches) {
                if (!match) {

                }
                corrections.push(`${match[1].replace("E", "Ä").replace("e", "ä")}hre${match[2]}`);
            }

            let reply: string;
            if (corrections.length == 1) {
                reply = `*${corrections[0]}`
            } else {
                reply = `\n*${corrections.join(`\n*`)}`
            }

            counter.inc(corrections.length);

            return message.reply(reply);
        },
    },
    {
        name: `Leibe ist für alle da`,
        description: `Denn \`Liebe\` ist einfach falsch`,
        match: /\bl[il1]eb/gmi,
        continueAfterExec: true,
        exec: async (handler, message) => {
            const counter = leibe.labels(message.author.id)
            const regex = /\b(l)([il1]e)(be?[^\s]*)\b/gmi;

            const matches = message.content.matchAll(regex)

            let corrections: string[] = []
            for (let match of matches) {
                if (!match) {

                }
                corrections.push(`${match[1]}${match[2].split('').reverse().join('')}${match[3]}`);
            }

            let reply: string;
            if (corrections.length == 1) {
                reply = `*${corrections[0]}`
            } else {
                reply = `\n*${corrections.join(`\n*`)}`
            }

            counter.inc(corrections.length);

            return message.reply(reply);
        },
    },
];
